'''Create csv with info for ECC pattern collection.
Separated from creating the ECC, due to difficulties
using both golly and lifelib in the same script.'''
import csv,sys
if '../..' not in sys.path:
    sys.path.insert(0, '../..')
from meyrin import conduit, utilities
with open('ecc-pattern-info.csv', 'w') as eccInfoCsv:
    writer = csv.DictWriter(eccInfoCsv, ['name', 'input', 'output', 'catalysts','envelope'])
    writer.writeheader()
    with open('../conduits/stable', 'r') as conduits:
        # assumes conduits/stable is already sorted.
        for line in conduits:
            if len(line.rstrip()) == 0: continue
            print(line.split(' #')[0])
            cond = conduit.DecodeConduit(line.split(' #')[0])
            if len(cond.outputs) > 0:
                # if 2 outputs, list twice, once for each output.
                for ind, out in enumerate(cond.outputs):
                    toWrite = {}
                    # it'd be nice if there was a way to include english name, eg syringe, snark, etc.
                    toWrite['name'] = line.split(' #')[1].rstrip().split(' ')[0]
                    toWrite['input'] = cond.input.getEncoding()
                    toWrite['output'] = out.getEncoding(cond.input.delta)
                    toWrite['catalysts'] = utilities.ShiftedAPGCode(cond.catalysts)
                    # I can't do just .apgcode. That only works for oscillators, spaceships, and glider guns.
                    # so use RLE: entry ends up being really lengthy (but oh well)
                    envelopePat = cond.getEnvelope(True, True)
                    x0, y0 = envelopePat.bounding_box[:2]
                    rleLines = envelopePat.rle_string().split('\n')
                    rawRLE = ''.join([line for line in rleLines
                                        if not (line.startswith('x =') or line.startswith('#'))])
                    toWrite['envelope'] = f'{rawRLE}@F{x0},{y0}'
                    writer.writerow(toWrite)