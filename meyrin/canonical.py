from meyrin import lt, orientation
from enum import Enum, auto

# TODO: rename via git mv

class CanonicalParticle(Enum):
    B = auto()
    C = auto()
    D = auto()
    E = auto()
    H6 = auto()
    H5 = auto()
    H3 = auto()
    H0 = auto()
    H_PRIME = auto()
    DEP_H_PRIME = auto()
    I = auto()
    J = auto()
    # O = auto() ?? 
    P = auto()
    Q = auto()
    R = auto()
    S = auto()
    # U = auto() u-turner, unused
    W = auto()
    G = auto()
    L = auto()
    M = auto()
    # N = auto() HWSS, unused

    @property
    def pat(self):
        return {"B": lt.pattern("o$bo$b2o$2o$o")(-1,-2),
            "C": lt.pattern("b3o$3bo$ob2o$2o")(-2,-1),
            "D": lt.pattern("2b2o$bobo$o2bo$obo$bo")(-2,-1),
            "E": lt.pattern("3o$2b2o$b2o!")(-2,-1),
            "H6": lt.pattern("bo$2b2o$3b2o$5bo$5o$3bo")(-5,-3),
            "H5": lt.pattern("bo$b3o$b3o$o3bo$4o$ob2o")(-4,-3),
            "H3": lt.pattern("b2o$3ob2o$b2ob3o$3ob2o$2o")(-5,-2),
            "H0": lt.pattern("o$obo$3o$2bo")(-1,-2),
            "H_PRIME": lt.pattern("2o$ob2o$3bo$bobo$b2o!")(3,-6),
            "DEP_H_PRIME": lt.pattern("""
2b3o$5o9bo$3ob2o7bo$5b3o4bo3b2o$4bo2bo4bo4b2o$4b2o6b2obo3bo$3b2o9bo5b
2o$3b2o15b2o$4bo2bo2bo8b2o$4bo2bobo3bo$3bo3bo4bob2o$13bob2o$15b2o!""")(4,-6),
            "I": lt.pattern("3b2o$bo2bo$o3bo$bobo$2bo")(-3,-1),
            "J": lt.pattern("3o$3b2o$3b2o!")(-2,-1),
            #"O": lt.pattern("b2o$2b2o$3o$o")(-2,-1),
            "P": lt.pattern("3o$2bo$3o")(-1,-1),
            "Q": lt.pattern("o$2o$b2o$b3o$b2o$2o$o")(-2,-3),
            "R": lt.pattern("bo$b2o$2o")(-1,-1),
            "S": lt.pattern("2bo$b2o$2o$o")(-1,-2),
            #"U": lt.pattern("2o$3o$2obo$2b2o")(-2,-2),
            "W": lt.pattern("b2o$2bo$2o$o")(-1,-1),
            "G": lt.pattern("bo$2bo$3o")(-1,-1),
            "L": lt.pattern("o2bo$4bo$o3bo$b4o")(-4,-2),
            "M": lt.pattern("2bo$o3bo$5bo$o4bo$b5o")(-5,-3),
            #"N": lt.pattern("2b2o$o4bo$6bo$o5bo$b6o")(-6,-3)
            }[self.name]
    
    def __str__(self):
        if self.name == 'H_PRIME':
            return 'H\''
        elif self.name == 'DEP_H_PRIME':
            return 'dep_H\''
        else:
            return self.name

    @property
    def InHSeq(self):
        return self.name in ['H6', 'H5', 'H3', 'H0', 'H_PRIME']
    
    @classmethod
    def HSeq(self):
        return [CanonicalParticle.H6, CanonicalParticle.H5, CanonicalParticle.H3,
                CanonicalParticle.H0, CanonicalParticle.H_PRIME]
    
    @property
    def InDepHSeq(self):
        return self.name in ['H6', 'H5', 'H3', 'DEP_H_PRIME']

    @classmethod
    def DepHSeq(self):
        return [CanonicalParticle.H6, CanonicalParticle.H5, CanonicalParticle.H3,
                CanonicalParticle.DEP_H_PRIME]

    @property
    def IsSpaceship(self):
        return self.name in ['G', 'L', 'M'] # 'N'

    @property
    def IsXWSS(self):
        return self.name in ['L', 'M'] # 'N'
    
    @classmethod
    def Spaceships(self):
        return [CanonicalParticle.G, CanonicalParticle.L, CanonicalParticle.M] # N
    
    @property
    def Sym(self):
        if self.name == 'G':
            return orientation.Symmetry.GLIDEDIAG
        elif self.IsXWSS:
            return orientation.Symmetry.GLIDEORTH
        if self.pat.centre() == self.pat('rot180').centre():
            return orientation.Symmetry.C2
        if self.pat == self.pat('flip_y'):
            return orientation.Symmetry.D2
        else:
            return orientation.Symmetry.NOSYM
    
    # (dt, transf, dx, dy) such that applying those left-to-right
    # to self.pat maps it to itself.
    @property
    def SpacetimeSym(self):
        if self.name == 'G':
            return (2, orientation.Orient.FLIP_Y, -1, 0)
        elif self.IsXWSS:
            return (2, orientation.Orient.SWAP_XY, -1, 0)
        if self.pat.centre() == self.pat('rot180').centre():
            return (0, orientation.Orient.ROT180, *self.pat.match(self.pat('rot180')).firstcell)
        if self.pat == self.pat('flip_y'):
            return (0, orientation.Orient.FLIP_Y, *self.pat.match(self.pat('rot180')).firstcell)
        else:
            return (0, orientation.Orient.IDENTITY, 0,0)
