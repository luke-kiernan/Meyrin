from meyrin import notations, lt, utilities, orientation

NUM_DESC = 12
MAX_GEN = 1000

def GetZOI(pat):
    square = lt.pattern('3o$3o$3o!')(-1, -1)
    return square.convolve(pat)

def GetHalo(pat):
    return GetZOI(pat) - pat

def PresentIn(subpat, pat):
    halo = GetHalo(subpat)
    return subpat <= pat and (halo & pat).empty()

def IsGlider(pat):
    for i in range(4):
        for o in orientation.tfns:
            if notations.canonical_particles['G'][i](o).centre() == pat.centre():
                return True
    return False

class ConduitException(Exception):
    def __init__(self, pat, diagnosticInfo = ""):
        super().__init__(self.MessageIntro()+"\n"+pat.rle_string())
        if len(diagnosticInfo) > 0:
            print("Diagnostic info:", end = ' ')
            for pair in diagnosticInfo.items():
                print(": ".join(map(str, pair)), end = ' ')
            print('')
class UnrecognizedObject(ConduitException):
    def MessageIntro(self):
        return "Unrecognized object:"
class BadCatalysts(ConduitException):
    def MessageIntro(self):
        return "Catalysts failed to recover or extra debris leftover:"
class OutputNotFound(ConduitException):
    def MessageIntro(self):
        return "Output not found:"
class MoreThanGliders(ConduitException):
    def MessageIntro(self):
        return "Pattern is X-to-G, yet when run for many generations, non-glider non-catalyst patterns remain: "

# TODO: make some test cases. Like unit tests! Beyond just test_conduit.py
# TODO: if input is D2- symmetric, correct so that first output has orientation among the rotations.
# TODO: 180 degree symmetry! stairstep!
class Object:
    def __init__(self, pat, gen = -1):
        """Initialize from pattern."""
        done = False
        for t in range(NUM_DESC): # check descendants, too: multiple gen 0 B variants.
            # order is important! if input is H0, need H0 to match before 3rd descendent of H3
            if pat[t].empty(): break
            for obj in notations.canonical_particles:
                if obj in notations.spaceships:
                    continue
                for o in orientation.preferred[notations.symmetryOfKind[obj]]:
                    rotated = notations.canonical_particles[obj][t](str(o))
                    imX, imY = pat[t].getrect()[:2]
                    rotX, rotY = rotated.getrect()[:2]
                    if rotated(imX-rotX, imY-rotY) == pat[t]:
                        self.kind = obj
                        self.orient = o
                        self.dx = imX-rotX
                        self.dy = imY-rotY
                        self.dt = 0
                        done = True
                        break # these "break" statements act as tiebreakers;
                if done: break # they matter for one conduit where input/output overlap.
            if done: break
        if not done:
            for spaceshipKind in notations.spaceships:
                numGens = 4 if spaceshipKind == 'G' else 2
                whichOriens = orientation.preferred[notations.symmetryOfKind[spaceshipKind]]
                for t in range(numGens):
                    for o in whichOriens:
                        if pat.centre() == notations.canonical_particles[spaceshipKind][t](str(o)).centre():
                            self.kind = spaceshipKind
                            self.orient = o
                            rewinded = pat[-t]
                            if spaceshipKind == 'G':
                                # dx, dy give the center of the glider in its canonical phase.
                                self.dx, self.dy = rewinded.bounding_box[0]+1, rewinded.bounding_box[1]+1
                            else:
                                toMatch = notations.canonical_particles[spaceshipKind](str(o))
                                self.dx, self.dy = rewinded.match(toMatch).bounding_box[:2]
                            self.dt = t
                            done = True
                            self.delta = 0
                            break
        if not done:
            raise UnrecognizedObject(pat)
        self.SetKindAndTime(self.kind, gen, -1)
        self.symmetric = pat('flip_y').centre() == pat.centre()
        self.blockRemoved = False # removed block that indicates dependent conduit.
    
    def SetKindAndTime(self, kind, gen, converges):
        self.kind = kind
        self.gen = gen
        self.convergesDelay = converges - gen
        self.H_seq = self.kind in notations.both_H_seq
        if self.H_seq:
        # TODO: replace "magical numbers" of gens
        # with lookups in a dictionary. same in gensAndDesc
            if self.kind == 'H\'':
                self.delta = 22
            elif self.kind == 'dep_H\'':
                self.delta = 50
            else:
                self.delta = -1*int(self.kind[1])
        else:
            self.delta = 0
        self.conventionalName = "H" if self.kind in notations.both_H_seq else self.kind

    def getEncoding(self, addToGen = 0):
        return notations.encode_output_obj(self.kind,self.gen + addToGen, self.orient, self.dx,
                                        self.dy, self.dt)
    
    def __str__(self):
        if self.kind in notations.spaceships:
            return f"kind {self.kind} gen {self.gen} location ({self.dx}, {self.dy}), phase {self.dt}"
        else:
            return f"kind {self.kind} gen {self.gen} location ({self.dx}, {self.dy}) orientation {self.orient}, converges after {self.convergesDelay} gens"

    # maybe this should return an Object...
    def SpecialGlider(self, name):
        """Return pattern, for where FNG/dep_FNG would be at generation 0.
        Keep in mind that if the object is an H3, this gives
        the FNG's would-be location when the H3 is present, not the H0."""
        if self.H_seq:
            return notations.specialGliders[name](str(self.orient))(self.dx, self.dy)[self.delta]
        return lt.pattern('')
    
    # canonical means all H sequence and dependent H sequence members collapse to H
    # noPrime means H' becomes H and dep_H' becomes H3, but pre-H's are okay.
    def getPattern(self, canonical = False, noPrime = False):
        # transform-then-step. for gliders, the difference matters!
        if not self.H_seq:
            return notations.canonical_particles[self.kind](str(self.orient))(self.dx, self.dy)[self.dt]
        if canonical or (noPrime and self.kind == "H\'"):
            return notations.canonical_particles['H0'](str(self.orient))(self.dx, self.dy)
        if noPrime and self.kind == "dep_H\'":
            return notations.canonical_particles['H3'](str(self.orient))(self.dx, self.dy)
        return notations.canonical_particles[self.kind](str(self.orient))(self.dx, self.dy)[self.dt]
        
    def getGen(self, canonical = False, noPrime = False):
        if self.H_seq and (canonical or (noPrime and self.kind == "H\'")):
            return self.gen - self.delta
        if noPrime and self.kind == "dep_H\'":
            return self.gen - self.delta - 3
        return self.gen
    
    def getConvergesGen(self, canonical = False, noPrime = False):
        return self.getGen(canonical, noPrime) + self.convergesDelay
    
    def getConvergesPattern(self, canonical = False, noPrime = False):
        return self.getPattern(canonical, noPrime)[self.convergesDelay]
    
def decodeObject(encoding):
    """Return output object with the given encoding."""
    info = notations.decode_output_obj(encoding)
    if len(info) == 6:
        (kind, gen, orient, dx, dy, phase) = info
    else:
        (kind, gen, orient, dx, dy) = info
        phase = 0
    pat = notations.canonical_particles[kind](str(orient))(dx,dy)[phase]
    return Object(pat, gen)

class Conduit:
    def FindOutputsRecursive(self, toStep, found, gen, errorInfo):
        # simplicity over efficiency here: this works with any number
        # of outputs, even if a later output requires FNG of earlier
        # one to form properly.
        while gen < MAX_GEN and not all(found):
            for outInd, out in enumerate(self.outputs):
                if not found[outInd] and out.H_seq:
                    for kind, pat in notations.both_H_seq.items():
                        transformed = pat(str(out.orient))(out.dx, out.dy)
                        if PresentIn(transformed, toStep):
                            newFound = found[:]
                            newFound[outInd] = True
                            newToStep = toStep - transformed
                            if kind == 'dep_H\'':
                                newToStep += out.SpecialGlider("dep_FNG")
                            if self.FindOutputsRecursive(newToStep, newFound, gen, errorInfo):
                                found[outInd] = True
                                errorInfo['never found'][outInd] = False
                                toStep -= transformed
                                out.SetKindAndTime(kind,gen,gen)
                                if kind == 'dep_H\'':
                                    toStep += out.SpecialGlider("dep_FNG")
                elif not found[outInd]:
                    for i in range(NUM_DESC):
                        if PresentIn(out.getPattern()[i], toStep):
                            newFound = found[:]
                            newFound[outInd] = True
                            newToStep = toStep - out.getPattern()[i]
                            if self.FindOutputsRecursive(newToStep, newFound, gen, errorInfo):
                                out.SetKindAndTime(out.kind, gen - i, gen)
                                found[outInd] = True
                                errorInfo['never found'][outInd] = False
                                toStep -= out.getPattern()[i]
            gen += 1
            toStep = toStep[1]
        if gen < MAX_GEN:
            # all outputs found, or none to find, so check recovery: catalysts present
            # and everything else is escaping gliders.
            toStepFF = toStep[MAX_GEN]
            if PresentIn(self.catalysts, toStepFF):
                leftovers = toStepFF - self.catalysts
                if not all([IsGlider(comp) for comp in leftovers.components()]) and len(self.outputs) == 0:
                    nonGliders = sum([comp for comp in leftovers.components() if not IsGlider(comp)], lt.pattern(''))
                    errorInfo['more than gliders'] = nonGliders
                    return False
                elif not all([IsGlider(comp) for comp in leftovers.components()]):
                    # go back and figure out debris when catalysts first recovered
                    presentInStreak = 0
                    for _ in range(MAX_GEN):
                        toStep = toStep[1]
                        if PresentIn(self.catalysts, toStep):
                            presentInStreak += 1
                        else:
                            presentInStreak = 0
                        if presentInStreak > 30:
                            errorInfo['debris'] = toStep - self.catalysts
                            break
                    return False
                elif len(self.gliders) == 0: # otherwise appends gliders twice if 2 outputs
                    for comp in leftovers.components():
                        self.gliders.append(Object(comp, gen + MAX_GEN))
                return True
            else:
                errorInfo['bad catalysts'] = toStepFF & GetZOI(self.catalysts)
                return False
                # "only gliders" might be too strict. Eg eat FNG with boat-bit reaction.
        return False

    def __init__(self, catPat, inputPat, outputPat):
        diagnosticInfo = {}
        if catPat[1] != catPat:
            raise BadCatalysts(catPat, diagnosticInfo)
        # center the input, using Object's initialization code.
        notCentered = Object(inputPat)
        inverseTransf = str(orientation.inverse(notCentered.orient))
        self.catalysts = catPat(-notCentered.dx, -notCentered.dy)(inverseTransf)
        transfOutput = outputPat(-notCentered.dx, -notCentered.dy)(inverseTransf)
        centeredInputPat = inputPat(-notCentered.dx, -notCentered.dy)(inverseTransf)
        # the result here might be H0 when it should be H3.
        self.input = Object(centeredInputPat, 0)
        assert(self.input.dx == 0 and self.input.dy == 0 and self.input.orient.value == 0)
        # TODO: it'd be nice if input recognition worked with ECC-style input,
        # ie functional input in on cells and H0 in marked cells,
        # instead of expecting the H0 in marked-and-on.
        if (self.input.H_seq and PresentIn(notations.depBlock, self.catalysts)) or \
                self.input.kind == 'dep_H\'':
            self.input.blockRemoved = True
            self.catalysts -= notations.depBlock
            # TODO: make sure dep_FNG is released.
            self.input.SetKindAndTime('dep_H\'',0,0)
        elif self.input.kind in notations.H_seq:
            # determine earliest interaction between H seq and catalysts
            diagnosticInfo = {"FNG used":False}
            testPat = self.catalysts + notations.canonical_particles["H6"]
            H6Copy = lt.pattern('')+notations.canonical_particles["H6"]
            gensAndDescs = {0:"H6", 1:"H5", 3:"H3", 6:"H0", 6+22:"H\'"}
            for gen, kind in gensAndDescs.items():
                if testPat[gen] == self.catalysts + H6Copy[gen]:
                    self.input.SetKindAndTime(kind,0,0)
                    if kind == "H\'": # reached last element
                        # make sure the FNG isn't used (if it is, correct to H0).
                        fng = self.input.SpecialGlider("FNG")
                        catsPlusFNG = self.catalysts + fng
                        t = 0
                        while t < MAX_GEN and fng <= catsPlusFNG:
                            fng = fng[1]
                            catsPlusFNG = catsPlusFNG[1]
                            t += 1
                        if t != MAX_GEN and catsPlusFNG[20] != self.catalysts:
                            diagnosticInfo["FNG used"] = True
                            self.input.SetKindAndTime("H0",0,0)
                        elif catsPlusFNG[20] == self.catalysts:
                            while (GetZOI(fng) & self.catalysts).empty():
                                fng = fng[1]
                            eater = self.catalysts.component_containing (GetZOI(fng) & self.catalysts)
                            if eater.population <= 7:
                                diagnosticInfo["eater removed"] = True
                                self.catalysts -= eater
                            else:
                                self.input.SetKindAndTime("H0",0,0)
                else:
                    break # interacted, so leave input as last-before-it-interacted.
        diagnosticInfo['input'] = self.input
        self.outputs = []
        self.gliders = []
        for comp in transfOutput.components('$'.join(7*['7o'])):
            try:
                obj = Object(comp)
            except UnrecognizedObject as e:
                outputFitsInside = comp | self.input.getPattern(canonical = True)
                done = False
                # this code relies on the iffy assumption there's only one object
                # that fits inside (red union white) and contains all the red cells
                for pat in notations.canonical_particles.values():
                    for o in orientation.tfns:
                        for coord in outputFitsInside.match(pat(o)).coords():
                            matched = pat(o)(coord[0], coord[1])
                            if comp <= matched and not (matched & \
                                                self.input.getPattern(canonical = True)).empty():
                                obj = Object(matched)
                                done = True
                                diagnosticInfo["overlap"] = True
                                break
                        if done: break
                    if done: break
                if done:
                    self.outputs.append(obj)
                else:
                    raise e
            else:
                if obj.kind != "G":
                    self.outputs.append(obj)
        errorInfo = {'never found':[True for _ in self.outputs], 'debris':'', 'bad catalysts':''}
        if not self.FindOutputsRecursive(
                    self.input.getPattern() + self.catalysts,
                    [False for _ in self.outputs], 0, errorInfo):
            if any(errorInfo['never found']):
                neverFound = self.outputs[errorInfo['never found'].index(True)].getPattern(canonical = True)
                raise OutputNotFound(neverFound, diagnosticInfo)
            elif 'more than gliders' in errorInfo:
                raise MoreThanGliders(errorInfo['more than gliders'], diagnosticInfo)
            elif 'debris' in errorInfo:
                raise BadCatalysts(errorInfo['debris'], diagnosticInfo)
            elif len(errorInfo['bad catalysts']) > 0:
                raise BadCatalysts(errorInfo['bad catalysts'],diagnosticInfo)
        # if the output isn't dep_H', then that means the dependent block is present
        # for connectability reasons (not for cleanup reasons) so remove.
        for out in self.outputs:
            if out.kind in ["H6", "H5", "H3"]:
                transfDepBlock = notations.depBlock(out.orient)(out.dx,out.dy)
                if PresentIn(transfDepBlock, self.catalysts):
                    self.catalysts -= transfDepBlock
                    out.blockRemoved = True
        self.outputs.sort(key = lambda obj : obj.gen)
        # go back and figure out when each glider first appears.
        # this way, object.gen means same thing for gliders and non-gliders.
        # TODO: I also need to do the same for XWSS outputs, too!
        toStep = self.getPattern(False)
        gensSet = [False for _ in self.gliders]
        glidersToCatch = [gl.getPattern()[-gl.gen] for gl in self.gliders]
        t, outputToFind = 0, 0
        while not all(gensSet):
            if outputToFind < len(self.outputs):
                nextOutputGen = self.outputs[outputToFind].getGen()
                if t == nextOutputGen:
                    toStep -= self.outputs[outputToFind].getPattern()
                    outputToFind += 1
            for ind, gl in enumerate(glidersToCatch):
                if (not gensSet[ind]) and PresentIn(gl[t], toStep):
                    # edge case: the glider's proximity perturbs some debris
                    # and the conduit doesn't recover otherwise.
                    sansGL = toStep - gl[t]
                    if sansGL[20] == toStep[20] - gl[t+20]:
                        toStep -= gl[t]
                        gensSet[ind] = True
                        if ind < len(self.gliders):
                            self.gliders[ind] = Object(gl[t], t)
            t += 1
            toStep = toStep[1]
            assert(t < MAX_GEN)
        self.gliders.sort(key = lambda obj : obj.gen)
    
    def Encoding(self):
        catsEncoded = utilities.ShiftedAPGCode(self.catalysts)
        outputsEncoded = " ".join([notations.encode_output_obj(out.kind, out.gen, out.orient,
                     out.dx, out.dy,out.dt) for out in self.outputs])
        glidersEncoded = " ".join([notations.encode_output_obj(gl.kind, gl.gen, gl.orient,
                     gl.dx, gl.dy,gl.dt) for gl in self.gliders])
        pieces = [self.input.kind, catsEncoded, outputsEncoded, glidersEncoded]
        while "" in pieces:
            pieces.remove("")
        return " ".join(pieces)
    
    def ShortName(self, includeGliders = False):
        delta = self.input.delta
        if len(self.outputs) == 0 and not includeGliders:
            includeGliders = True # len(self.outputs) > 0 is really "output isn't G"
        starInOrient = self.input.symmetric and len(self.outputs) == 1
        outNames = [notations.non_ss_short_name(out.kind, out.gen+delta-out.delta, out.orient, starInOrient)
                    if out.kind not in notations.XWSS_kinds
                    else notations.ss_short_name(out.kind, out.gen+delta, out.orient,out.dx, out.dy,out.dt)
                    for out in sorted(self.outputs, key = lambda item : item.gen)]
        if includeGliders:
            outNames += [notations.ss_short_name(gl.kind, gl.gen+delta,gl.orient, gl.dx, gl.dy, gl.dt)
                            for gl in self.gliders]
        toJoin = outNames[:]
        toJoin[0] = self.input.conventionalName + toJoin[0]
        normalName = "_".join(toJoin)

        if self.input.kind in ["H3", "H5", "H6", "dep_H\'"]:
            normalName += " dependent (input)"
        if any([out.kind == "dep_H\'" for out in self.outputs]):
            normalName +=" dependent form"
        return normalName
    
    def getCatalysts(self, conventional = True):
        catalysts = self.catalysts + lt.pattern('')
        if conventional:
            for out in self.outputs:
                if out.blockRemoved:
                    catalysts += notations.depBlock(out.orient)(out.dx,out.dy)
            if self.input.blockRemoved:
                catalysts += notations.depBlock
        return catalysts

    def getPattern(self, conventional = True, eatFNG = False):
        if not conventional:
            return self.catalysts + self.input.getPattern()
        if not eatFNG:
            return self.input.getPattern(False, True) + self.getCatalysts(conventional)
        return self.input.getPattern(False, True) + self.getCatalysts(conventional) + notations.fngEater

    def CorrectForInput(self, num, conventional):
        if conventional:
            if self.input.kind == "H\'":
                num += self.input.delta # conventional input is H0
            elif self.input.kind == "dep_H\'":
                num += self.input.delta+3 # conventional input is H3
        return num
    
    def CalculateNextOutputGen(self, outputToFind, conventional):
        nextOutputGen = self.outputs[outputToFind].getConvergesGen(False, conventional)
        if self.outputs[outputToFind].kind == "dep_H\'":
            nextOutputGen = self.outputs[outputToFind].getConvergesGen()
        return self.CorrectForInput(nextOutputGen, conventional)

    def getEnvelope(self, conventional = True, eatFNG = False):
        # the possible +3 from CorrectForInput affects all outputs the same.
        self.outputs.sort(key = lambda x : x.getConvergesGen(False, conventional))
        toStep = self.getPattern(conventional)
        glidersToCatch = [gl.getPattern()[-gl.gen+self.input.getGen(False, conventional)]
                                                                     for gl in self.gliders]
        if self.input.kind == "H\'" and conventional:
            glidersToCatch.append(notations.specialGliders["FNG"])
        t, outputToFind = 0, 0
        caughtAtTimes = [-1 for _ in glidersToCatch]
        envelope = lt.pattern('') + toStep
        toAdd = {} # store FNG of output H in here if it's too early to add them in.
        # (rewinding then adding in at time of output would add extra to envelope)
        while True:
            if outputToFind < len(self.outputs):
                # if dep FNG of output required for recovery, we evolve until dep_H'
                # is present, even if conventional = true--connecting conduit shouldn't overlap.
                nextOutputGen = self.CalculateNextOutputGen(outputToFind, conventional)

                while t == nextOutputGen: # could have multiple outputs on same gen.
                    if self.outputs[outputToFind].kind == "dep_H\'":
                        toAdd[t+33] = self.outputs[outputToFind].SpecialGlider("dep_FNG")[33]
                        assert(PresentIn(self.outputs[outputToFind].getConvergesPattern(), toStep))
                        toStep -= self.outputs[outputToFind].getConvergesPattern()
                    else:
                        if not PresentIn(self.outputs[outputToFind] \
                                        .getConvergesPattern(False, conventional), toStep):
                            print(toStep.rle_string())
                        assert(PresentIn(self.outputs[outputToFind] \
                                        .getConvergesPattern(False, conventional), toStep))
                        toStep -= self.outputs[outputToFind]\
                                        .getConvergesPattern(False, conventional)
                    if self.outputs[outputToFind].kind == "H\'":
                        toAdd[t + 23] = self.outputs[outputToFind].SpecialGlider("FNG")[1]
                    outputToFind += 1
                    if outputToFind == len(self.outputs):
                        break
                    nextOutputGen = self.CalculateNextOutputGen(outputToFind, conventional)
            if t in toAdd:
                toStep += toAdd[t]
                toAdd.pop(t)
            t += 1
            toStep = toStep[1]
            envelope += toStep
            if t % 10 == 0 and -1 in caughtAtTimes:
                for ind, glider in enumerate(glidersToCatch):
                    if ind < len(self.gliders):
                        effGliderGen = self.CorrectForInput(self.gliders[ind].gen, conventional)
                    else:
                        effGliderGen = 0
                    if caughtAtTimes[ind] == -1 and PresentIn(glider[t], toStep) and t >= effGliderGen:
                        toStep -= glider[t]
                        caughtAtTimes[ind] = t
            if outputToFind == len(self.outputs) and self.getCatalysts(conventional) == toStep:
                break
            # debugging stuff.
            if t == MAX_GEN and outputToFind < len(self.outputs):
                print("missing outputs")
            if t == MAX_GEN and -1 in caughtAtTimes:
                print("missing gliders")
            assert(t < MAX_GEN)
        # add back in each glider, and step long enough that there's
        # a visible glider trail sticking out of the envelope.
        LONG_ENOUGH = 30
        for ind,gl in enumerate(glidersToCatch):
            t = caughtAtTimes[ind]
            timeOutsideEnvelope = 0
            while timeOutsideEnvelope < LONG_ENOUGH:
                if gl[t] <= envelope:
                    timeOutsideEnvelope = 0
                else:
                    timeOutsideEnvelope += 1
                envelope += gl[t]
                t += 1
        self.outputs.sort(key = lambda x : x.getGen())
        return envelope
    
def DecodeConduit(encoding):
    inputKind, catInfo = encoding.split(' ')[:2]
    apgCode, orient = catInfo.split('@')
    rest = ''
    if orient[1] == 'x':
        transf = orientation.OrientFor(orient[:2],orientation.Symmetry.NOSYM)
        rest = orient[2:]
    else:
        transf = orientation.OrientFor(orient[0],orientation.Symmetry.NOSYM)
        rest = orient[1:]
    x,y = map(lambda x : int(x), rest.split(','))
    if apgCode == "xp0_0":
        cats = lt.pattern('')
    else:
        cats = lt.pattern(apgCode)(str(transf))(x,y)

    inputPat = notations.canonical_particles[inputKind]

    firstOutput, lastOutput = 2,2
    pieces = encoding.split(' ')
    while lastOutput < len(pieces) and notations.decode_output_obj(pieces[lastOutput])[0] != 'G':
        lastOutput += 1
    outputPat = lt.pattern('')
    for i in range(firstOutput, lastOutput):
        outputPat += decodeObject(encoding.split(' ')[i]).getPattern(True)

    return Conduit(cats, inputPat, outputPat)
