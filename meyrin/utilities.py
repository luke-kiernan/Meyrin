from meyrin import lt, notations, orientation

def ShiftedAPGCode(pat):
    if pat.empty():
        return "xp0_0@F0,0"
    apgcode = pat.apgcode
    apgPat = lt.pattern(apgcode)
    for o in orientation.preferred[orientation.Symmetry.NOSYM]:
        if pat.centre() == apgPat(str(o)).centre():
            x0, y0 = apgPat(str(o)).bounding_box[:2]
            xf, yf = pat.bounding_box[:2]
            xCats, yCats = xf - x0, yf - y0
            oCats = o
            break
    return f"{apgcode}@{orientation.OcodeFor(orientation.Symmetry.NOSYM, oCats)}{xCats},{yCats}"