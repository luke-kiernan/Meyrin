from meyrin import notations, orientation
from itertools import product

# check encode/decode are inverses.

gens, lanes, timings = range(-5,5), range(-5,5), range(-5,5)
# gliders
for ocode in ['NE', 'NW', 'SE', 'SW']:
    for gen, time, lane in product(gens, timings, lanes):
        encoded = f'{gen}:{ocode}{lane}T{time}'
        decoded = notations.decode_output_obj(encoded)
        assert(notations.encode_output_obj(*decoded) == encoded)
# XWSSes
for ocode in ['N', 'S', 'E', 'W', 'Nx', 'Sx', 'Ex', 'Wx']:
    for gen, time, lane in product(gens, timings, lanes):
        encoded = f'{gen}:{ocode}{lane}T{time}Lw'
        decoded = notations.decode_output_obj(encoded)
        assert(notations.encode_output_obj(*decoded) == encoded)
# non spaceships.
testPatKinds = {
    orientation.Symmetry.NOSYM:'B',
    orientation.Symmetry.C2:'S',
    orientation.Symmetry.D2:'P'
}
for sym,kind in testPatKinds.items():
    for orient in orientation.preferred[sym]:
        ocode = orientation.OcodeFor(orient, sym)
        gens = range(-5,5)
        dxes = range(-5,5)
        dyes = range(-5,5)
        for gen, dx, dy in product(gens, dxes, dyes):
            encoded = f"{gen}:{kind}@{ocode}{dx},{dy}"
            decoded = notations.decode_output_obj(encoded)
            assert(notations.encode_output_obj(*decoded) == encoded)
    
