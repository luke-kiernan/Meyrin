from meyrin import lt, canonical, orientation

# check composition
Bpat = canonical.CanonicalParticle.B.pat
for orient1 in orientation.Orient:
    for orient2 in orientation.Orient:
        composition = orient1.postCompose(orient2)
        assert(Bpat(str(composition)) == (Bpat(str(orient1)))(str(orient2)))

# check inverse
for orient in orientation.Orient:
    assert(orient.inverse().postCompose(orient)
            == orientation.Orient(0) and 
            orient.postCompose(orient.inverse())
            == orientation.Orient(0))

subgroups = {
    orientation.Symmetry.NOSYM:[orientation.Orient(0)],
    orientation.Symmetry.C2:[orientation.Orient(0), orientation.Orient(2)],
    orientation.Symmetry.D2:[orientation.Orient(0), orientation.Orient(4)],
    orientation.Symmetry.GLIDEDIAG:[orientation.Orient(0), orientation.Orient(5)],
    orientation.Symmetry.GLIDEORTH:[orientation.Orient(0)]
}

# check that each coset is in preferred exactly once.
for sym, subgrp in subgroups.items():
    if len(subgrp) > 1:
        otherTransf = subgrp[1]
        pref = sym.PreferredOrients
        nonPref = set([o.postCompose(subgrp[1]) for o in pref])
        assert(len(set(pref).intersection(nonPref)) == 0)
        assert(len(set(pref).union(nonPref)) == 8)

# check that for preferred orients, OrientFor and OcodeFor are inverses.
for sym in orientation.Symmetry:
    for o in sym.PreferredOrients:
        assert(sym.OrientFor(sym.OcodeFor(o)) == o)

sizes = {
    orientation.Symmetry.NOSYM:1,
    orientation.Symmetry.C2:2,
    orientation.Symmetry.D2:2,
    orientation.Symmetry.GLIDEDIAG:2,
    orientation.Symmetry.GLIDEORTH:1
}

testPats = {
    orientation.Symmetry.NOSYM:canonical.CanonicalParticle.B.pat,
    orientation.Symmetry.C2:lt.pattern('o$5o$4bo!').centre(),
    orientation.Symmetry.D2:canonical.CanonicalParticle.P.pat,
    orientation.Symmetry.GLIDEDIAG:lt.pattern('4o$o2bo$o$2o!'),
    orientation.Symmetry.GLIDEORTH:canonical.CanonicalParticle.L.pat
}

for sym in orientation.Symmetry:
    unionOfCosets = set()
    for coset in orientation.inv_ocode_cosets[sym].values():
        assert(len(unionOfCosets.intersection(set(coset))) == 0)
        unionOfCosets.update(set(coset))
        assert(len(coset) == sizes[sym])
        for item in coset:
            assert(testPats[sym](str(coset[0])) == testPats[sym](str(item)))
