import sys
if '..' not in sys.path:
    sys.path.insert(0, '..')
from meyrin import canonical, lt

NUM_DESC = 12
MAX_GEN = 1000

class UnrecognizedObject(Exception):
    def __init__(self, pat):
        super().__init__("Unrecognized object:\n"+pat.rle_string())


class Particle:
    def __init__(self, pat, gen = None):
        """Initialize from pattern."""
        done = False
        for t in range(NUM_DESC): # check descendants, too: multiple gen 0 B variants.
            # order is important! if input is H0, need H0 to match before 3rd descendent of H3
            if pat[t].empty(): break
            for obj in canonical.CanonicalParticle:
                if obj.IsSpaceship:
                    continue
                for o in obj.Sym.PreferredOrients:
                    rotated = obj.pat[t](str(o))
                    imX, imY = pat[t].getrect()[:2]
                    rotX, rotY = rotated.getrect()[:2]
                    if rotated(imX-rotX, imY-rotY) == pat[t]:
                        self.kind = obj
                        self.orient = o
                        self.dx = imX-rotX
                        self.dy = imY-rotY
                        self.dt = 0
                        done = True
                        break # these "break" statements act as tiebreakers;
                if done: break # they matter for one conduit where input/output overlap.
            if done: break
        if not done:
            for ss in canonical.CanonicalParticle.Spaceships():
                numGens = 4 if ss.name == 'G' else 2
                for t in range(numGens):
                    for o in ss.Sym.PreferredOrients:
                        if pat.centre() == ss.pat[t](str(o)).centre():
                            self.kind = ss
                            self.orient = o
                            rewinded = pat[-t]
                            if ss.name == 'G':
                                # dx, dy give the center of the glider in its canonical phase.
                                self.dx, self.dy = rewinded.bounding_box[0]+1, rewinded.bounding_box[1]+1
                            else:
                                toMatch = ss.pat(str(o))
                                self.dx, self.dy = rewinded.match(toMatch).bounding_box[:2]
                            self.dt = t
                            done = True
                            self.delta = 0
                            break
        if not done:
            raise UnrecognizedObject(pat)
        self.SetKindAndTime(self.kind, gen, None)
        self.blockRemoved = False # removed block that indicates dependent conduit.
    
    def SetKindAndTime(self, kind, gen, converges):
        self.kind = kind
        self.gen = gen
        if converges is not None and self.gen is not None:
            self.convergesDelay = converges - gen
        else:
            self.convergesDelay = None
        if self.kind.InHSeq or self.kind.InDepHSeq:
        # TODO: replace "magical numbers" of gens
        # with lookups in a dictionary. same in gensAndDesc
            self.conventionalName = "H"
            if self.kind.name == 'H_PRIME':
                self.delta = 22
            elif self.kind.name == 'DEP_H_PRIME':
                self.delta = 50
            else:
                self.delta = -1*int(self.kind.name[1])
        else:
            self.delta = 0
            self.conventionalName = self.kind.name

    def getEncoding(self, addToGen = 0):
        ocode = self.kind.Sym.OcodeFor(self.orient)
        # if not self.orient in self.kind.Sym.PreferredOrients:
        #    raise(f"Unexpected orientation for a particle of kind {self.kind.name}")
        if self.kind.name == 'G':
            # (dx,dy) is the pre-step translation,
            # ie the center of the glider dt generations ago.
            xDirection = 1 if str(self.orient) in ['identity', 'flip_y'] else -1
            if str(self.orient) in ['identity', 'rot180']:
            # negative sign: diagram at https://conwaylife.com/forums/viewtopic.php?t=1849#p23178
                y0 = -1*(self.dy - self.dx)
            else:
                y0 = self.dx + self.dy
            t0 = (self.gen - self.dt) - 4*self.dx*xDirection
            return f"{self.gen+addToGen}:{ocode}{y0}T{t0}"
        elif self.kind.IsXWSS:
            # gens passed since crossed origin = 
            # 2*projection of (dx, dy) onto axis parallel to direction of travel
            # lane = projection of (dx, dy) onto axis perp to direction of travel
            canonicalPhaseGen = self.gen - self.dt
            if ocode[0] in ['N', 'S']:
                timing = canonicalPhaseGen + (-2*self.dy if ocode[0] == 'S' else 2*self.dy)
                lane = self.dx
            if ocode[0] in ['E', 'W']:
                # negative sign: earlier diagram.
                timing = canonicalPhaseGen + (-2*self.x if ocode[0] == 'E' else 2*self.dx)
                lane = -self.dy
            return f"{self.gen+addToGen}:{ocode}{lane}T{timing}{str(self.kind)}"
        else:
            return f"{self.gen+addToGen}:{str(self.kind)}@{ocode}{self.dx},{self.dy}"
    
    def ShortName(self, addToGen = 0):
        if self.kind.IsSpaceship:
            return self.getEncoding(addToGen).split[':'][1] # addToGen actually doesn't matter here.
        name = "H" if self.kind.InHSeq or self.kind.InDepHSeq else self.kind.name
        ocode = self.kind.Sym.OcodeFor(self.orient)
        return f"{ocode}{self.gen+addToGen}{name}"

    def __str__(self):
        if self.kind in canonical.spaceships:
            return f"kind {str(self.kind)} gen {self.gen} location ({self.dx}, {self.dy}), phase {self.dt}"
        else:
            return f"kind {str(self.kind)} gen {self.gen} location ({self.dx}, {self.dy}) orientation {self.orient}, converges after {self.convergesDelay} gens"


    def SpecialGlider(self):
        """Return pattern, for where FNG/dep_FNG would be at generation 0.
        Keep in mind that if the object is an H3, this gives
        the FNG's would-be location when the H3 is present, not the H0."""
        # where they are when H0 is/would be present
        fng = lt.pattern('2bo$2o$b2o!')(4,-8)
        dep_fng = lt.pattern('bo$o$3o!')(26,-18)
        if self.kind.InHSeq:
            return fng(str(self.orient))(self.dx, self.dy)[self.delta]
        elif self.kind.InDepHSeq:
            return dep_fng(str(self.orient))(self.dx, self.dy)[self.delta]
        return lt.pattern('')
    
    # canonical means all H sequence and dependent H sequence members collapse to H
    # noPrime means H' becomes H and dep_H' becomes H3, but pre-H's are okay.
    # (I could replace these extra options with separate functions.)
    def getPattern(self, canonical = False, noPrime = False):
        # transform-then-step. for gliders, the difference matters!
        if not (self.kind.InHSeq or self.kind.InDepHSeq):
            return self.kind.pat(str(self.orient))(self.dx, self.dy)[self.dt]
        if canonical or (noPrime and self.kind.name == 'H_PRIME'):
            return canonical.CanonicalParticle.H0.pat(str(self.orient))(self.dx, self.dy)
        if noPrime and self.kind.name == "DEP_H_PRIME":
            return canonical.CanonicalParticle.H3.pat(str(self.orient))(self.dx, self.dy)
        return self.kind.pat(str(self.orient))(self.dx, self.dy)[self.dt]
        
    def getGen(self, canonical = False, noPrime = False):
        if (self.kind.InHSeq or self.kind.InDepHSeq) and \
                (canonical or (noPrime and self.kind.name == "H_PRIME")):
            return self.gen - self.delta
        if noPrime and self.kind.name == "DEP_H_PRIME":
            return self.gen - self.delta - 3
        return self.gen
    
    def getConvergesGen(self, canonical = False, noPrime = False):
        return self.getGen(canonical, noPrime) + self.convergesDelay
    
    def getConvergesPattern(self, canonical = False, noPrime = False):
        return self.getPattern(canonical, noPrime)[self.convergesDelay]

def DecodeParticle(encoded):
    """Reverse of getEncoding."""
    gen, rest = int(encoded.split(':')[0]), encoded.split(':')[1]
    if '@' in encoded: # non-spaceship
        kindName, rest = rest.split('@')
        kind = canonical.CanonicalParticle[kindName]
        assert(kind not in canonical.CanonicalParticle.spaceships)
        dir_code = rest[0] if (rest[1] == '-' or rest[1].isnumeric()) else rest[:2]
        orient = kind.Sym.OrientFor(dir_code)
        dx, dy = map(lambda x : int(x), rest[len(dir_code):].split(','))
        return (kind, gen, orient, dx, dy, 0) # TODO: change return type to particle?
    elif rest[:2] in ['NE', 'NW', 'SE','SW']:
        kind =  canonical.CanonicalParticle.G
        y0, t0 = map(lambda x:int(x), rest[2:].split('T'))
        # negative sign: earlier diagram.
        if rest[:2] in ['SE', 'NW']:
            y0 *= -1
        orient = kind.Sym.OrientFor(rest[:2])
        phase = (gen - t0) % 4
        if phase < 0:
            phase += 4
        pre_phase = canonical.CanonicalParticle.G.pat(str(orient))(0, y0)[gen-t0-phase]
        dx, dy = pre_phase.bounding_box[0]+1, pre_phase.bounding_box[1]+1
        return (kind, gen, orient, dx, dy, phase)
    else:
        # assumes output, so order is [direction][lane]T[timing][kind]
        # (for input, we use the CanonicalParticle, so no need for lane-and-timing)
        kind = canonical.CanonicalParticle[rest[-1]]
        rest = rest[:-1]
        assert(kind.IsXWSS)
        t0 = int(rest.split('T')[1])
        ocodeAndLane = rest.split('T')[0]
        ocode = ocodeAndLane[0] if ocodeAndLane[1] == '-' or ocodeAndLane[1].isnumeric() else ocodeAndLane[:2]
        orient = kind.Sym.OrientFor(ocode)
        lane = int(ocodeAndLane[len(ocode):])
        if ocode[0] in ['N', 'S']:
            shift = (lane,0)
            direction = (0,1) if ocode[0] == 'S' else (0,-1)
        else:
            # negative sign: earlier diagram.
            shift = (0,-lane)
            direction = (1,0) if ocode[0] == 'E' else (-1,0)
        pos = []
        phase = (gen - t0) % 4
        if phase < 0: phase += 4
        for i in range(2):
            pos.append(shift[i]+direction[i]*(gen-phase-t0)//2)
        return (kind, gen, orient, *pos, phase)