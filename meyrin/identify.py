import sys
if '/Users/lukekiernan/Documents/misc-non-school-projects/Meyrin' not in sys.path:
   sys.path.insert(0, '/Users/lukekiernan/Documents/misc-non-school-projects/Meyrin')
from meyrin import conduit, lt

# TODO: it'd be nice if this objected to duplicates.
# ie same verbatim conduit twice. As I'm compiling lists of 
# RLEs, it'd be a form of input checking.

def LifeHistoryToSixPats(rle):
   '''Given LifeHistory RLE, returns dictionary of j:[pattern obj of state j cells]'''
   newLineInd = rle.find('\n')
   rulePart, patPart = rle[:newLineInd], rle[newLineInd+1:]
   patterns = [patPart.replace('.', 'b') for _ in range(6)]
   letters = ['A','B', 'C', 'D', 'E', 'F']
   for i in range(6):
      for j in range(6):
         if j != i:
            patterns[i] = patterns[i].replace(letters[j], 'b')
         else:
            patterns[i] = patterns[i].replace(letters[j], 'o')
      patterns[i] = rulePart.replace('LifeHistory', 'B3/S23') + '\n' + patterns[i]
   return {i+1:lt.pattern(patterns[i]) for i in range(6)}

rles = []
currentRLE = ""
for line in sys.stdin:
    if line.startswith("x ="):
        if currentRLE != "":
           rles.append(currentRLE)
        currentRLE = line
    elif not line.startswith('#'):
        currentRLE += line.rstrip()
if currentRLE != "":
    rles.append(currentRLE)
names = []
for rle in rles:
    assert("rule = LifeHistory" in rle)
    pats = LifeHistoryToSixPats(rle)
    try:
       cond = conduit.Conduit(pats[1]+pats[5], pats[3], pats[4])
    except Exception as e:
       print("Double check the following RLE:")
       print(rle)
       raise e
    inputAndOutput = (cond.input.conventionalName,
          'G' if len(cond.outputs) == 0 else cond.outputs[0].conventionalName)
    names.append((cond.Encoding(), cond.ShortName(), inputAndOutput))
names.sort(key = lambda tp : (tp[2], tp[1],tp[0])) # group by types then by common name.
i = 0
while i < len(names): # deal with repeats in the conventional way.
   if i + 1 < len(names) and names[i+1][1] == names[i][1]:
      originalName = names[i][1]
      j = i
      letter = 'a'
      while j < len(names) and names[j][1] == originalName:
         words = names[j][1].split(' ') # in case there's extra adjectives
         words[0] += letter # like "dependent form" at the end.
         names[j] = (names[j][0], ' '.join(words))
         j += 1
         letter = chr(ord(letter)+1)
      i = j
   else:
      names[i] = (names[i][0], names[i][1])
      i += 1
for name in names:
   print(" #".join(name)) # encoding, with short name as a comment.