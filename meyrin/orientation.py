from enum import IntEnum

class Orient(IntEnum):
    IDENTITY = 0
    RCW = 1 # from x towards y
    ROT180 = 2
    RCCW = 3
    FLIP_Y = 4 # flips sign of y coord; reflects across x-axis
    SWAP_XY_FLIP = 5
    FLIP_X = 6
    SWAP_XY =  7

    def __str__(self):
        return f'{self.name.lower()}'

    def inverse(self):
        if self < 4:
                return Orient((4-self)%4)
        return Orient(self)
    
    def postCompose(self, other):
        # t = flip across x, r = 90 deg cw rotation. read right to left.
        if other < 4 and self < 4: # r^a * r^b
            return Orient((other + self) % 4)
        if other >= 4 and self >= 4: # t r^a * t r^b
            return Orient((self-other+4)%4)
        if other < 4 and self >= 4: # r^a * t r^b
            return Orient((self-other)%4 + 4)
        if other >= 4 and self < 4:# t r^a * r^b
            return Orient((other+self)%4 + 4)

tfns = [str(orient) for orient in Orient]

class Symmetry(IntEnum):
    NOSYM = 0
    D2 = 1 # across x-axis: for pi, qb
            # cosets: {'F':(0,4), 'R':(1,7), 'B':(2,6), 'L':(3,5)}
    C2 = 2 # for stairstep:
            # note that stairstep is C2 about (0, -0.5), not (0,0), though.
            # cosets: {'S':(0,2), 'T':(1,3), 'Sx':(4,6), 'Tx':(5,7)}
    GLIDEDIAG = 3 # for glider
            # cosets: {'SE':(0,7), 'SW':(6,1), 'NW':(2,5), 'NE':(4,3)}
    GLIDEORTH = 4 # for XWSSes
            # due to the unreduced speed, the quotient here is trivial.
            # a reflected XWSS does not cover the same set of spacetime coords
            # as a non-reflected XWSS.
    
    @property
    def PreferredOrients(self):
        if self ==  Symmetry.NOSYM:
            return tuple([Orient(i) for i in range(8)])
        if self == Symmetry.D2:
            return tuple([Orient(i) for i in range(4)])
        if self == Symmetry.C2:
            return (Orient(0), Orient(1), Orient(4), Orient(5))
        if self == Symmetry.GLIDEDIAG:
            return tuple([Orient(i) for i in range(0,8,2)])
        if self == Symmetry.GLIDEORTH:
            return tuple([Orient(i) for i in range(8)])

    def OcodeFor(self, orient):
        if self == Symmetry.NOSYM:
            return ['F', 'R', 'B', 'L', 'Fx', 'Rx', 'Bx', 'Lx'][orient]
        if self == Symmetry.D2:
            if orient == 4:
                return 'F'
            return ['F', 'R', 'B', 'L'][min(orient, 8-orient)]
        if self == Symmetry.C2:
            if orient < 4:
                return ['S', 'T'][orient % 2]
            else:
                return ['Sx', 'Tx'][orient % 2]
        if self == Symmetry.GLIDEDIAG:
            k = orient if orient % 2 == 0 else 7 - orient
            return ['SE', 'NW', 'NE', 'SW'][k // 2]
        if self == Symmetry.GLIDEORTH:
            return ['E', 'S', 'W', 'N', 'Ex', 'Sx', 'Wx', 'Nx'][orient]
    
    def OrientFor(self, ocode):
        if self == Symmetry.NOSYM:
            return Orient(['F', 'R', 'B', 'L', 'Fx', 'Rx', 'Bx', 'Lx'].index(ocode))
        if self == Symmetry.D2:
            return Orient(['F', 'R', 'B', 'L'].index(ocode))
        if self == Symmetry.C2:
            if ocode in ['S', 'T']:
                return Orient(['S', 'T'].index(ocode))
            return Orient(['Sx', 'Tx'].index(ocode)+4)
        if self == Symmetry.GLIDEDIAG:
            if ocode in ['SE', 'NW']:
                return Orient(2*(['SE', 'NW'].index(ocode)))
            return Orient(4+2*(['NE', 'SW']).index(ocode))
        if self == Symmetry.GLIDEORTH:
            return Orient(['E', 'S', 'W', 'N', 'Ex', 'Sx', 'Wx', 'Nx'].index(ocode))

# unused in code now, but still useful for debugging.
inv_ocode_cosets = {
    Symmetry.NOSYM:{'F':(Orient(0),), 'R':(Orient(1),), 'B':(Orient(2),), 'L':(Orient(3),),
                'Fx':(Orient(4),), 'Rx':(Orient(5),), 'Bx':(Orient(6),), 'Lx':(Orient(7),)},
    Symmetry.D2:{'F':(Orient(0),Orient(4)), 'R':(Orient(1),Orient(7)),
                'B':(Orient(2),Orient(6)), 'L':(Orient(3),Orient(5))},
    Symmetry.C2:{'S':(Orient(0),Orient(2)), 'T':(Orient(1),Orient(3)),
                 'Sx':(Orient(4),Orient(6)), 'Tx':(Orient(5),Orient(7))},
    Symmetry.GLIDEDIAG:{'SE':(Orient(0),Orient(7)), 'SW':(Orient(6),Orient(1)), # convention: choose reflect
                 'NW':(Orient(2),Orient(5)), 'NE':(Orient(4),Orient(3))}, # across y instead of rotate cw.
    Symmetry.GLIDEORTH:{'E':(Orient(0),), 'S':(Orient(1),), 'W':(Orient(2),), 'N':(Orient(3),),
                'Ex':(Orient(4),), 'Sx':(Orient(5),), 'Wx':(Orient(6),), 'Nx':(Orient(7),)}
}