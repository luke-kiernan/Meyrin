Keeping the namesake of the original repository, this is a work-in-progress version of a searchable database of conduits. It is also intended as a stepping stone towards a Herserch-type program, that searches for closed circuits.

Due to this end goal, some of my choices deviate from the standard notation. I regard H as a _sequence_: H6 -> H5 -> H3 -> H0 -> H' + FNG, instead of a single region. Each transition from an earlier stage to a later one is a conduit with no catalysts. Input (for an H-to-X conduit) is the latest accepted entry point; output (for an X-to-H conduit) is the earliest accepted entry point. This way, a table of what-connects-to-what tells us all we need to know, regardless of whether the conduit is a typical FNG-emitting conduit or something more special. For example, the dependent form of the syringe converts a glider to an H6, but it fails to connect to the H6-to-H' conduit.

Similarly, dependent conduits follow the sequence H3 -> dep_H', with a transparent block acts as a catalyst for the first step. The dependent H' then releases the a glider, which I call the dependent FNG. Unlike the regular H, the dependent H' forms *before* the dependent FNG is released.

As a result of these changes, the number of generations from input to output in the long form encodings may be different than usual. Short names, on the other hand, follow the usual conventions for input and output--they only include H, not H' or H3. `getPattern()` methods have an argument called `conventional` that determines whether the output follows my internal representation, or the usual conventions.

Long-form encodings remain the same as the original: for example, consider `H5 xs36_woe1e8zx11d96y4ooz8e13zy9bd@Rx12,8 48:H6@R-1,22 `.
* the first part is the input: `H5`, a herschel, which interacts with the conduit 5 generations before it reaches its standard phase (at the standard position).
* the middle part is the the stable stuff in conduit: the apgcode `xs36_woe1e8zx11d96y4ooz8e13zy9bd`, in orientation R shifted by (12, 8).
* the last part is the output(s): output is gen 48, and it's a 6 generation herschel predecessor in orientation R, shifted by (-1, 22).
This is the conduit that's usually known as [HR49H](https://conwaylife.com/wiki/R49). If any gliders were emitted, those would also be included, in the standard [lane-and-timing format](https://conwaylife.com/wiki/NW31).

There are a half-dozen conduits that are purposefully omitted: they are special cases that identify.py isn't equiped to handle yet. These are listed under conduits/omitted.